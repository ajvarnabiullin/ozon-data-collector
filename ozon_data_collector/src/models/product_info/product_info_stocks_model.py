from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductInfoStocksModel(BaseModel):
    __tablename__ = 'product_info_stocks'

    coming = Column(Integer, default=0)
    present = Column(Integer, default=0)
    reserved = Column(Integer, default=0)

    product_info_id = Column(Integer, ForeignKey('product_info.pk'))