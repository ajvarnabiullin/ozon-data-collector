from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float, ARRAY
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductInfoStatusModel(BaseModel):

    __tablename__ = 'product_info_status'

    state = Column(String, default='')
    state_failed = Column(String, default='')
    moderate_status = Column(String, default='')
    decline_reasons = Column(ARRAY(String), default=[])
    validation_state = Column(String, default='')
    state_name = Column(String, default='')
    state_description = Column(String, default='')
    is_failed = Column(Boolean)
    is_created = Column(Boolean)
    state_tooltip = Column(String, default='')
    item_errors = relationship('ProductInfoStatusItemErrorModel', cascade="all, delete-orphan")
    state_updated_at = Column(DateTime)

    product_info_id = Column(Integer, ForeignKey('product_info.pk'))