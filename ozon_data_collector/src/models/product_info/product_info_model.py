from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float, ARRAY
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductInfoModel(BaseModel):
    """Модель для /product/info"""
    __tablename__ = 'product_info'
    
    barcode = Column(String, default='')
    buybox_price = Column(String, default='')
    category_id = Column(Integer, default=0)
    color_image = Column(String, default='')
    comissions = relationship('ProductInfoComissionModel', cascade="all, delete-orphan")
    created_at = Column(DateTime)
    fbo_sku = Column(Integer, default=0)
    fbs_sku = Column(Integer, default=0)
    # id = Column(Integer, default=0)
    images = Column(ARRAY(String), default=[])
    primary_image = Column(String, default='')
    image360 = Column(ARRAY(String), default=[])
    is_prepayment = Column(Boolean)
    is_prepayment_allowed = Column(Boolean)
    marketing_price = Column(String, default='')
    min_ozon_price = Column(String, default='')
    min_price = Column(String, default='')
    name = Column(String, default='')
    offer_id = Column(String, default='')
    old_price = Column(String, default='')
    premium_price = Column(String, default='')
    price = Column(String, default='')
    price_index = Column(String, default='')
    recommended_price  = Column(String, default='')
    status = relationship('ProductInfoStatusModel', cascade="all, delete-orphan", uselist=False)
    sources = relationship('ProductInfoSourceModel', cascade="all, delete-orphan")
    stocks = relationship('ProductInfoStocksModel', cascade="all, delete-orphan", uselist=False)
    vat = Column(String, default='')
    visibility_details  = relationship('ProductInfoVisibilityDetailsModel', cascade="all, delete-orphan", uselist=False)
    visible = Column(Boolean)
    volume_weight = Column(Float, default=0)


    seller_id = Column(Integer, ForeignKey('seller.pk'), nullable=True)