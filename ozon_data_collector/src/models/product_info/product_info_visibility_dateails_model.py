from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductInfoVisibilityDetailsModel(BaseModel):
    __tablename__ = 'product_info_visibility_details'

    active_product = Column(Boolean)
    has_price = Column(Boolean)
    has_stock = Column(Boolean)

    product_info_id = Column(Integer, ForeignKey('product_info.pk'))