from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime


class OptionalDescriptionElementsModel(BaseModel):
    __tablename__ = 'optional_description_elements'
    property_name = Column(String, default='')

    product_info_status_item_error = Column(Integer, ForeignKey('product_info_status_item_error.pk'))

class ProductInfoStatusItemErrorModel(BaseModel):
    __tablename__ = 'product_info_status_item_error'

    code = Column(String, default='')
    state = Column(String, default='')
    level = Column(String, default='')
    description = Column(String, default='')
    field = Column(String, default='')
    attribute_id = Column(Integer, default=0)
    attribute_name = Column(String, default='')
    optional_description_elements = relationship('OptionalDescriptionElementsModel',  cascade="all, delete-orphan",  uselist=False)

    product_info_status_id = Column(Integer, ForeignKey('product_info_status.pk'))
    product_info_list_status_id = Column(Integer, ForeignKey('product_info_list_status.pk'))

