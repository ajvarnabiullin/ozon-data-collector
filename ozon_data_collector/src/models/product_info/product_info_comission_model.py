from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductInfoComissionModel(BaseModel):
    """"""
    __tablename__ = 'product_info_comission'

    deliveryAmoun = Column(Float, default=0)
    minValue = Column(Float, default=0)
    percent = Column(Float, default=0)
    returnAmount = Column(Float, default=0)
    saleSchema = Column(String, default='')
    value = Column(Float, default=0)

    product_info_id = Column(Integer, ForeignKey('product_info.pk'))