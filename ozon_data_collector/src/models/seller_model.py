from .base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String
from datetime import datetime
from sqlalchemy.orm import relationship

class Seller(BaseModel):
    """Class for seller information"""
    __tablename__ = 'seller'

    client_id = Column(String, default='')
    api_key = Column(String, default='')

    analytics_stock_on_warehouse = relationship('AnalyticsStockOnWarehouseModel', cascade="all, delete-orphan")
    
    product_info = relationship('ProductInfoModel', cascade="all, delete-orphan")

    posting_fbo_list = relationship('PostingFBOListModel', cascade="all, delete-orphan")

    product_list = relationship('ProductListModel', cascade="all, delete-orphan")
    
    product_info_list = relationship('ProductInfoListModel', cascade="all, delete-orphan")

    product_info_stocks = relationship('ProductInfoStocksBaseModel', cascade="all, delete-orphan")

    product_info_stocks_by_warehouse = relationship('ProductInfoStocksByWarehouseFBSModel',  cascade="all, delete-orphan")

    posting_fbs_list = relationship('PostingFBSListModel',  cascade="all, delete-orphan")