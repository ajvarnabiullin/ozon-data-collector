from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float, BigInteger
from sqlalchemy.orm import relationship
from datetime import datetime

class FinanceTransactionListResponseOperationModel(BaseModel):
    __tablename__ = 'finance_transaction_list_response_operations'

    accruals_for_sale = Column(Float, default=0)
    amount = Column(Float, default=0)
    delivery_charge = Column(Float, default=0)
    items = relationship('FinanceTransactionListResponseOperationItemModel', cascade="all, delete-orphan")
    operation_date = Column(DateTime)
    operation_id = Column(BigInteger, default=0)
    operation_type = Column(String, default='')
    operation_type_name = Column(String, default='')

    posting = relationship('FinanceTransactionListResponseOperationPostingModel', cascade="all, delete-orphan", uselist=False,)

    return_delivery_charge = Column(Float, default=0)
    sale_commission = Column(Float, default=0)

    services = relationship('FinanceTransactionListResponseOperationServicesModel', cascade="all, delete-orphan")

    type = Column(String, default='')

    finance_transaction_list_id = Column(Integer, ForeignKey('finance_transaction_list.pk'))