from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class FinanceTransactionListModel(BaseModel):
    __tablename__ = 'finance_transaction_list'

    operations = relationship('FinanceTransactionListResponseOperationModel', cascade="all, delete-orphan")
    page_count = Column(Integer, default=0)
    row_count = Column(Integer, default=0)