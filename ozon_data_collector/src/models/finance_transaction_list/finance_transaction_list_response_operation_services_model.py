from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class FinanceTransactionListResponseOperationServicesModel(BaseModel):
    __tablename__ = 'finance_transaction_list_response_operation_services'

    name = Column(String, default='')
    price = Column(Float, default=0)

    finance_transaction_list_response_operations_id = Column(Integer, ForeignKey('finance_transaction_list_response_operations.pk'))