from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class FinanceTransactionListResponseOperationItemModel(BaseModel):
    __tablename__ = 'finance_transaction_list_response_operation_item'

    name = Column(String, default='')
    sku = Column(Integer, default=0)

    finance_transaction_list_response_operations_id = Column(Integer, ForeignKey('finance_transaction_list_response_operations.pk'))