from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class FinanceTransactionListResponseOperationPostingModel(BaseModel):
    __tablename__ = 'finance_transaction_list_response_operation_posting'

    delivery_schema = Column(String, default='')
    order_date = Column(String, default='')
    posting_number = Column(String, default='')
    warehouse_id = Column(Integer, default=0)

    finance_transaction_list_response_operations_id = Column(Integer, ForeignKey('finance_transaction_list_response_operations.pk'))