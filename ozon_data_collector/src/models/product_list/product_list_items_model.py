from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductListItemsModel(BaseModel):
    __tablename__ = 'product_list_items'

    offer_id = Column(String, default='')
    product_id = Column(Integer, default=0)

    product_list_id = Column(Integer, ForeignKey('product_list.pk'))