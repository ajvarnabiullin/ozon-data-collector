from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductInfoStocksItemStocksModel(BaseModel):
    __tablename__ = 'product_info_stocks_item_stocks'

    present = Column(Integer, default=0)
    reserved = Column(Integer, default=0)
    type = Column(String, default='')

    product_info_stocks_item_id = Column(Integer, ForeignKey('product_info_stocks_item.pk'))