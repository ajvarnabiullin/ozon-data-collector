from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime


class ProductInfoStocksItemModel(BaseModel):
    __tablename__ = 'product_info_stocks_item'

    offer_id = Column(String, default='')
    product_id = Column(Integer, default=0)
    stocks = relationship('ProductInfoStocksItemStocksModel',  cascade="all, delete-orphan")

    product_info_stocks_id = Column(Integer, ForeignKey('product_info_stocks_base.pk'))