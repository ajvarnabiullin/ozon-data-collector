from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductInfoListVisibilityDetailsReasonsReasonsModel(BaseModel):
    __tablename__ = 'product_info_list_visibility_details_reasons_reasons'
    
    description = Column(String, default='')
    # id = Column(Integer, default=0)
    
    product_info_list_visibility_details_reasons_id = Column(Integer, ForeignKey('product_info_list_visibility_details_reasons.pk'))

class ProductInfoListVisibilityDetailsReasonsModel(BaseModel):
    __tablename__ = 'product_info_list_visibility_details_reasons'

    property_name = relationship('ProductInfoListVisibilityDetailsReasonsReasonsModel', cascade="all, delete-orphan", uselist=False)

    product_info_list_visibility_details_id = Column(Integer, ForeignKey('product_info_list_visibility_details.pk'))