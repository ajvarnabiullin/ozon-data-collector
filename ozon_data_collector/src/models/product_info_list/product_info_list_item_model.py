from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float, ARRAY
from sqlalchemy.orm import relationship
from datetime import datetime


class ProductInfoListItemModel(BaseModel):
    """	
        Array of objects (productv2GetProductInfoListResponseItem)
        Массив данных.
    """
    __tablename__ = 'product_info_list_item'
    
    barcode = Column(String, default='')
    buybox_price = Column(String, default='')
    category_id = Column(Integer, default=0)
    color_image = Column(String, default='')
    created_at = Column(DateTime)
    fbo_sku = Column(Integer, default=0)
    fbs_sku = Column(Integer, default=0)
    # id = Column(Integer, default=0)
    images = Column(ARRAY(String), default=[])
    primary_image = Column(String, default='')
    images360 = Column(ARRAY(String), default=[])
    marketing_price = Column(String, default='')
    min_ozon_price = Column(String, default='')
    min_price = Column(String, default='')
    name = Column(String, default='')
    offer_id = Column(String, default='')
    old_price = Column(String, default='')
    premium_price = Column(String, default='')
    price = Column(String, default='')
    price_index = Column(String, default='')
    recommended_price = Column(String, default='')
    status = relationship('ProductInfoListStatusModel', cascade="all, delete-orphan", uselist=False)
    sources = relationship('ProductInfoListSourceModel', cascade="all, delete-orphan")
    stocks = relationship('ProductInfoListStocksModel', cascade="all, delete-orphan", uselist=False)
    vat = Column(String, default='')
    visibility_details = relationship('ProductInfoListVisibilityDetailsModel', cascade="all, delete-orphan", uselist=False)
    visible = Column(Boolean)

    product_info_list_id = Column(Integer, ForeignKey('product_info_list.pk'))