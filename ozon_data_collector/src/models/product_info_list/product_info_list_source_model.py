from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductInfoListSourceModel(BaseModel):
    __tablename__ = 'product_info_list_source'

    is_enabled = Column(Boolean)
    sku = Column(Integer, default=0)
    source = Column(String, default='')
    
    product_info_list_item_id = Column(Integer, ForeignKey('product_info_list_item.pk'))