from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductInfoListVisibilityDetailsModel(BaseModel):
    __tablename__ = 'product_info_list_visibility_details'

    active_product = Column(Boolean)
    has_price = Column(Boolean)
    has_stock = Column(Boolean)
    reasons = relationship('ProductInfoListVisibilityDetailsReasonsModel', cascade="all, delete-orphan", uselist=False)

    product_info_list_item_id = Column(Integer, ForeignKey('product_info_list_item.pk'))
