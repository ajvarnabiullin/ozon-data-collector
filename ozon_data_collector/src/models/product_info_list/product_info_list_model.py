from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductInfoListModel(BaseModel):
    __tablename__ = 'product_info_list'

    items = relationship('ProductInfoListItemModel', cascade="all, delete-orphan")

    seller_id = Column(Integer, ForeignKey('seller.pk'), nullable=True)