from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingFinancialDataModel(BaseModel):
    __tablename__ = 'posting_fbs_list_posting_financial_data'

    posting_services = relationship('PostingFBSListPostingFinancialDataPostingServicesModel', cascade="all, delete-orphan", uselist=False)
    products = relationship('PostingFBSListPostingFinancialDataProductModel', cascade="all, delete-orphan")

    posting_fbs_list_posting_id = Column(Integer, ForeignKey('posting_fbs_list_posting.pk'))