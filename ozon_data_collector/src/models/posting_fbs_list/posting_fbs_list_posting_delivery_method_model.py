from email.policy import default
from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingDeliveryMethodModel(BaseModel):
    __tablename__ = 'posting_fbs_list_posting_delivery_method'

    id = Column(Integer, default=0)
    name = Column(String, default='')
    tpl_provider = Column(String, default='')
    tpl_provider_id = Column(Integer, default=0)
    warehouse = Column(String, default='')
    warehouse_id = Column(Integer, default=0)

    posting_fbs_list_posting_id = Column(Integer, ForeignKey('posting_fbs_list_posting.pk'))