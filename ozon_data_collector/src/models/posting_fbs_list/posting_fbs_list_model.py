from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListModel(BaseModel):
    __tablename__ = 'posting_fbs_list'

    has_next = Column(Boolean)
    postings = relationship('PostingFBSListPostingModel', cascade="all, delete-orphan")

    seller_id = Column(Integer, ForeignKey('seller.pk'), nullable=True)
