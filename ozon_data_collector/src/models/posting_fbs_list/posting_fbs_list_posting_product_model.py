from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float, ARRAY
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingProductModel(BaseModel):
    __tablename__ = 'posting_fbs_list_posting_product'

    mandatory_mark = Column(ARRAY(String), default=[])
    name = Column(String, default=[])
    offer_id = Column(String, default=[])
    price = Column(String, default=[])
    quantity = Column(Integer, default=0)
    sku = Column(Integer, default=0)

    posting_fbs_list_posting_id = Column(Integer, ForeignKey('posting_fbs_list_posting.pk'))