from email.policy import default
from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime


class PostingFBSListPostingBarcodesModel(BaseModel):
    __tablename__ = 'posting_fbs_list_posting_barcodes'

    lower_barcode = Column(String, default='')
    upper_barcode = Column(String, default='')

    posting_fbs_list_posting_id = Column(Integer, ForeignKey('posting_fbs_list_posting.pk'))