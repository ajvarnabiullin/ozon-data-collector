from ast import For
from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingFinancialDataProductPickingModel(BaseModel):
    __tablename__ = 'posting_fbs_list_posting_financial_data_product_picking'

    amount = Column(Float, default=0)
    moment = Column(String, default='')
    tag = Column(String, default='')

    posting_fbs_list_posting_financial_data_product_id = Column(Integer, ForeignKey('posting_fbs_list_posting_financial_data_product.pk'))