from email.policy import default
from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingCancellation(BaseModel):
    __tablename__ = 'posting_fbs_list_posting_cancellation'

    affect_cancellation_rating = Column(Boolean)
    cancel_reason = Column(String, default='')
    cancel_reason_id = Column(Integer, default=0)
    cancellation_initiator = Column(String, default='')
    cancellation_type = Column(String, default='')
    cancelled_after_ship = Column(Boolean)

    posting_fbs_list_posting_id = Column(Integer, ForeignKey('posting_fbs_list_posting.pk'))