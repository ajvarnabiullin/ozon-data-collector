from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float, ARRAY
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingFinancialDataProductModel(BaseModel):
    __tablename__ = 'posting_fbs_list_posting_financial_data_product'

    actions = Column(ARRAY(String))
    client_price = Column(String, default='')
    commission_amount = Column(Float, default=0)
    commission_percent = Column(Integer, default=0)
    item_services = relationship('PostingFBSListPostingFinancialDataPostingServicesModel', cascade="all, delete-orphan", uselist=False)
    old_price = Column(Float, default=0)
    payout = Column(Float, default=0)
    picking = relationship('PostingFBSListPostingFinancialDataProductPickingModel', cascade="all, delete-orphan", uselist=False)
    price = Column(Float, default=0)
    product_id = Column(Integer, default=0)
    quantity = Column(Integer, default=0)
    total_discount_percent = Column(Float, default=0)
    total_discount_value = Column(Float, default=0)

    posting_fbs_list_posting_financial_data_id = Column(Integer, ForeignKey('posting_fbs_list_posting_financial_data.pk'))