from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingCustomerAddressModel(BaseModel):
    __tablename__ = 'posting_fbs_list_posting_customer_address'

    address_tail = Column(String, default='')
    city = Column(String, default='')
    comment = Column(String, default='')
    country = Column(String, default='')
    district = Column(String, default='')
    latitude = Column(Float, default=0)
    longitude = Column(Float, default=0)
    provider_pvz_code = Column(String, default='')
    pvz_code = Column(Integer, default=0)
    region = Column(String, default='')
    zip_code = Column(String, default='')

    posting_fbs_list_posting_customer_id = Column(Integer, ForeignKey('posting_fbs_list_posting_customer.pk'))