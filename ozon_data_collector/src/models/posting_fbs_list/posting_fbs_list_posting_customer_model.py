from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingCustomerModel(BaseModel):
    __tablename__ = 'posting_fbs_list_posting_customer'

    address = relationship('PostingFBSListPostingCustomerAddressModel', cascade="all, delete-orphan", uselist=False)
    customer_email = Column(String, default='')
    customer_id = Column(Integer, default=0)
    name = Column(String, default='')
    phone = Column(String, default='')

    posting_fbs_list_posting_id = Column(Integer, ForeignKey('posting_fbs_list_posting.pk'))

