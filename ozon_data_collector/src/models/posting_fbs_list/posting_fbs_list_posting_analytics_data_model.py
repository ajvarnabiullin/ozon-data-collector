from email.policy import default
from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime


class PostingFBSListAnalyticsDataModel(BaseModel):
    __tablename__ = 'posting_fbs_list_analytics_data'

    city = Column(String, default='')
    delivery_date_begin = Column(DateTime)
    delivery_date_end = Column(DateTime)
    delivery_type = Column(String, default='')
    is_legal = Column(Boolean)
    is_premium = Column(Boolean)
    payment_type_group_name = Column(String, default='')
    region = Column(String, default='')
    tpl_provider = Column(String, default='')
    tpl_provider_id = Column(Integer, default=0)
    warehouse = Column(String, default='')
    warehouse_id = Column(Integer, default=0)
    
    posting_fbs_list_posting_id = Column(Integer, ForeignKey('posting_fbs_list_posting.pk'))