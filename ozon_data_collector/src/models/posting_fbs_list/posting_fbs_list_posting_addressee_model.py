from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingAddresseeModel(BaseModel):
    __tablename__ = 'posting_fbs_list_posting_addressee'

    name = Column(String, default='')
    phone = Column(String, default='')

    posting_fbs_list_posting_id = Column(Integer, ForeignKey('posting_fbs_list_posting.pk'))