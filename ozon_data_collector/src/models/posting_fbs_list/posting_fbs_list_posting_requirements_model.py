from ..base import BaseModel
from sqlalchemy import ARRAY, Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingRequirementsModel(BaseModel):
    _tablename__ = 'posting_fbs_list_posting_requirements'

    products_requiring_gtd = Column(ARRAY(String), default=[])
    products_requiring_country = Column(ARRAY(String), default=[])
    products_requiring_mandatory_mark = Column(ARRAY(String), default=[])

    posting_fbs_list_posting_id = Column(Integer, ForeignKey('posting_fbs_list_posting.pk'))