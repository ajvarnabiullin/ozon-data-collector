from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBSListPostingModel(BaseModel):
    __tablename__ = 'posting_fbs_list_posting'

    addressee = relationship('PostingFBSListPostingAddresseeModel', cascade="all, delete-orphan", uselist=False)
    analytics_data = relationship('PostingFBSListAnalyticsDataModel', cascade="all, delete-orphan", uselist=False)
    barcodes = relationship('PostingFBSListPostingBarcodesModel', cascade="all, delete-orphan", uselist=False)
    cancellation = relationship('PostingFBSListPostingCancellation', cascade="all, delete-orphan", uselist=False)
    customer = relationship('PostingFBSListPostingCustomerModel', cascade="all, delete-orphan", uselist=False)
    delivering_date = Column(DateTime)
    delivery_method = relationship('PostingFBSListPostingDeliveryMethodModel', cascade="all, delete-orphan", uselist=False)
    financial_data = relationship('PostingFBSListPostingFinancialDataModel', cascade="all, delete-orphan", uselist=False)
    in_process_at = Column(String, default='')
    is_express = Column(Boolean)
    order_id = Column(Integer, default=0)
    order_number = Column(String, default='')
    posting_number = Column(String, default='')
    products = relationship('PostingFBSListPostingProductModel', cascade="all, delete-orphan")
    requirements = relationship('PostingFBSListPostingRequirementsModel', cascade="all, delete-orphan", uselist=False)
    shipment_date = Column(DateTime)
    status = Column(String, default='')
    tpl_integration_type = Column(String, default='')
    tracking_number = Column(String, default='')

    posting_fbs_list_id = Column(Integer, ForeignKey('posting_fbs_list.pk'))