from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBOListAnalyticsDataModel(BaseModel):
    __tablename__ = 'posting_fbo_list_analytics_data'

    city = Column(String, default='')
    delivery_type = Column(String, default='')
    is_legal = Column(Boolean)
    is_premium = Column(Boolean)
    payment_type_group_name = Column(String, default='')
    region = Column(String, default='')
    warehouse_id = Column(Integer, default=0)
    warehouse_name = Column(String, default='')

    posting_fbo_list_id = Column(Integer, ForeignKey('posting_fbo_list.pk'))