from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBOListModel(BaseModel):
    __tablename__ = 'posting_fbo_list'

    additional_data = relationship('PostingFBOListAdditionalDataModel', cascade="all, delete-orphan")
    analytics_data = relationship('PostingFBOListAnalyticsDataModel', cascade="all, delete-orphan", uselist=False)
    cancel_reason_id = Column(Integer, default=0)
    created_at = Column(DateTime)
    financial_data = relationship('PostingFBOListFinancialDataModel', cascade="all, delete-orphan", uselist=False)
    in_process_at = Column(String, default='')
    order_id = Column(Integer, default=0)
    order_number = Column(String, default='')
    posting_number = Column(String, default='')
    products = relationship('PostingFBOListProductModel', cascade="all, delete-orphan")
    status = Column(String, default='')

    seller_id = Column(Integer, ForeignKey('seller.pk'), nullable=True)
