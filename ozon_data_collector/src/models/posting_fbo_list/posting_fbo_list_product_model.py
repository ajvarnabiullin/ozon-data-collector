from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBOListProductModel(BaseModel):
    __tablename__ = 'posting_fbo_list_product'

    digital_code = Column(String, default='')
    name = Column(String, default='')
    offer_id = Column(String, default='')
    price = Column(String, default='')
    quantity = Column(Integer, default=0)
    sku = Column(Integer, default=0)

    posting_fbo_list_id = Column(Integer, ForeignKey('posting_fbo_list.pk'))