from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBOListFinancialDataPostingServicesModel(BaseModel):
    __tablename__ = 'posting_fbo_list_financial_data_posting_services'

    marketplace_service_item_deliv_to_customer = Column(Float, default=0)
    marketplace_service_item_direct_flow_trans = Column(Float, default=0)
    marketplace_service_item_dropoff_ff = Column(Float, default=0)
    marketplace_service_item_dropoff_pvz = Column(Float, default=0)
    marketplace_service_item_dropoff_sc = Column(Float, default=0)
    marketplace_service_item_fulfillment = Column(Float, default=0)
    marketplace_service_item_pickup = Column(Float, default=0)
    marketplace_service_item_return_after_deliv_to_customer = Column(Float, default=0)
    marketplace_service_item_return_flow_trans = Column(Float, default=0)
    marketplace_service_item_return_not_deliv_to_customer = Column(Float, default=0)
    marketplace_service_item_return_part_goods_customer = Column(Float, default=0)

    posting_fbo_list_financial_data_product_id = Column(Integer, ForeignKey('posting_fbo_list_financial_data_product.pk'))

    posting_fbo_list_financial_data_id = Column(Integer, ForeignKey('posting_fbo_list_financial_data.pk'))