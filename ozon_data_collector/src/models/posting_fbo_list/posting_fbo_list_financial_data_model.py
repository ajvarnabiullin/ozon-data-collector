from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBOListFinancialDataModel(BaseModel):
    __tablename__ = 'posting_fbo_list_financial_data'
    
    posting_services = relationship('PostingFBOListFinancialDataPostingServicesModel', cascade="all, delete-orphan", uselist=False)
    products = relationship('PostingFBOListFinancialDataProductModel', cascade="all, delete-orphan")

    posting_fbo_list_id = Column(Integer, ForeignKey('posting_fbo_list.pk'))