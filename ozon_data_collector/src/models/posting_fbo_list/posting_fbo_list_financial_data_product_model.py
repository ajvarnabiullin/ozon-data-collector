from mailbox import Babyl
from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float, ARRAY
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBOListFinancialDataProductModel(BaseModel):
    __tablename__ = 'posting_fbo_list_financial_data_product'

    actions = Column(ARRAY(String), default=[])
    client_price = Column(String, default='')
    commission_amount = Column(Float, default=0)
    commission_percent = Column(Integer, default=0)
    item_services = relationship('PostingFBOListFinancialDataPostingServicesModel', cascade="all, delete-orphan", uselist=False)
    old_price = Column(Float, default=0)
    payout = Column(Float, default=0)
    picking = relationship('ProductFBOListFinancialDataProductPickingModel', cascade="all, delete-orphan", uselist=False)
    price = Column(Float, default=0)
    product_id = Column(Integer, default=0)
    quantity = Column(Integer, default=0)
    total_discount_percent = Column(Float, default=0)
    total_discount_value = Column(Float, default=0)

    posting_fbo_list_financial_data_id = Column(Integer, ForeignKey('posting_fbo_list_financial_data.pk'))