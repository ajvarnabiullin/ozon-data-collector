from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class PostingFBOListAdditionalDataModel(BaseModel):
    __tablename__ = 'posting_fbo_list_additional_data'

    key = Column(String, default='')
    value = Column(String, default='')

    posting_fbo_list_id = Column(Integer, ForeignKey('posting_fbo_list.pk'))