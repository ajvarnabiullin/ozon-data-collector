from mailbox import Babyl
from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

class ProductFBOListFinancialDataProductPickingModel(BaseModel):
    __tablename__ = 'product_fbo_llist_financial_data_ product_pickings'

    amount = Column(Float, default=0)
    moment = Column(String, default='')
    tag = Column(String, default='')

    posting_fbo_list_financial_data_product_id = Column(Integer, ForeignKey('posting_fbo_list_financial_data_product.pk'))