from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime


class ProductInfoStocksByWarehouseFBSModel(BaseModel):
    __tablename__ = 'product_info_stocks_by_warehouse_fbs'

    fbs_sku = Column(Integer, default=0)
    present = Column(Integer, default=0)
    product_id = Column(Integer, default=0)
    reserved = Column(Integer, default=0)
    warehouse_id = Column(Integer, default=0)
    warehouse_name = Column(String, default='')

    seller_id = Column(Integer, ForeignKey('seller.pk'), nullable=True)