from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String
from datetime import datetime
from sqlalchemy.orm import relationship


class AnalyticsWhItemModel(BaseModel):
    """Данные остатков по определённым складам."""
    __tablename__ = 'analytics_wh_item'

    custom_id = Column(String, default='')
    name = Column(String, default='')
    
    items = relationship('AnalyticsWhItemItemModel', cascade="all, delete-orphan")
    analytics_stock_on_warehouse_id = Column(Integer, ForeignKey('analytics_stock_on_warehouse.pk'), default=None)
