from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String
from datetime import datetime
from sqlalchemy.orm import relationship


class AnalyticsWhItemItemStockModel(BaseModel):
    """object (AnalyticsGetStockOnWarehousesResponseItemStock)"""
    __tablename__ = 'analytics_wh_item_item_stock'

    for_sale = Column(Integer, default=0)
    loss = Column(Integer, default=0)
    not_for_sale = Column(Integer, default=0)


    analytics_wh_item_item_id = Column(Integer, ForeignKey('analytics_wh_item_item.pk'))
