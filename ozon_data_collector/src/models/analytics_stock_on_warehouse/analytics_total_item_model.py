from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Float
from datetime import datetime
from sqlalchemy.orm import relationship


class AnalyticsTotalItemModel(BaseModel):
    """Данные по остаткам на всех складах."""
    __tablename__ = 'analytics_total_item'
    
    barcode = Column(String, default='')
    category = Column(String, default='')
    discounted = Column(String, default='')
    height = Column(Float, default=0)
    length = Column(Float, default=0)
    offer_id = Column(String, default='')
    sku = Column(String, default='')
    title = Column(String, default='')
    volume = Column(Float, default=0)
    weight = Column(Float, default=0)
    width = Column(Float, default=0)

    stocks = relationship('AnalyticsTotalItemStocksModel', uselist=False, cascade="all, delete-orphan")
    analytics_stock_on_warehouse_id = Column(Integer, ForeignKey('analytics_stock_on_warehouse.pk'))

