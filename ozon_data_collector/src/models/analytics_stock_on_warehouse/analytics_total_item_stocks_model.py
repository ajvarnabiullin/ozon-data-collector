from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String
from datetime import datetime
from sqlalchemy.orm import relationship

class AnalyticsTotalItemStocksModel(BaseModel):
    """Информация об остатках товаров."""
    __tablename__ = 'analytics_total_item_stock'

    between_warehouses = Column(Integer, default=0)
    for_sale = Column(Integer, default=0)
    loss = Column(Integer, default=0)
    not_for_sale = Column(Integer, default=0)
    shipped = Column(Integer, default=0)

    analytics_total_item_id = Column(Integer, ForeignKey('analytics_total_item.pk'))
