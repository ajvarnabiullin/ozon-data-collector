from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from datetime import datetime

class AnalyticsStockOnWarehouseModel(BaseModel):
    """Отчёт по остаткам и товарам"""
    __tablename__ = 'analytics_stock_on_warehouse'
    # __table_args__ = {u'schema': 'bm'}

    timestamp = Column(DateTime(), default=datetime.now)
    total_items = relationship('AnalyticsTotalItemModel', cascade="all, delete-orphan")
    wh_items = relationship('AnalyticsWhItemModel', cascade="all, delete-orphan")

    seller_id = Column(Integer, ForeignKey('seller.pk'), nullable=True)


