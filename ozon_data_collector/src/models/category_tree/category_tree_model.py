from ..base import BaseModel
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String, Boolean, Float
from sqlalchemy.orm import relationship
from datetime import datetime

# class CategoryTreeModel(BaseModel):
#     __tablename__ = 'category_tree'

#     category_id = Column(Integer, default=0)
#     children: List['CategoryTree'] = []
#     title = Column(String, default='')