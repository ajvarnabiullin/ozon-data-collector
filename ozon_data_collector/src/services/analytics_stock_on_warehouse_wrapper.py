from .base import BaseWrapper
from ozon_db.orm import ComparisonsSchemesToModels

from ..models.base import BaseModel
from typing import TypeVar, Type
from pydantic import BaseModel as PBaseModel
from ozon_sdk.ozon_api import OzonApi
import asyncio
from ..models import AnalyticsStockOnWarehouseModel, AnalyticsTotalItemModel, AnalyticsWhItemModel, AnalyticsWhItemItemModel, AnalyticsTotalItemStocksModel, AnalyticsWhItemItemStockModel
from ozon_sdk.response import AnalyticsStockOnWarehouseResponse
from ozon_sdk.entities import TotalItem, WhItem
from ozon_sdk.entities.total_item import Stock as total_item_stock
from ozon_sdk.entities.wh_item import Item as wh_item_item
from ozon_sdk.entities.wh_item import Stock as wh_item_stock

TBaseModel = TypeVar('TBaseModel', bound=BaseModel)
TResponse = TypeVar('TResponse', bound=PBaseModel)

class AnalyticsOnStockWarehouseWrapper(BaseWrapper):
    """Сохранение Отчёт по остаткам и товарам в бд"""

    def __init__(self, client_id, api_key):
        super().__init__(client_id=client_id, api_key=api_key)

        self._comparisons_schemes_to_models = {

        }
        self._comparisons = self._set_comparisons_schemes_to_models(self._comparisons_schemes_to_models)

    
    async def get_response(self, request_args: dict = {}) -> TResponse:
        """Получение аргументов для создания запросов"""

        analytics_stock_on_warehouse = asyncio.create_task(self._ozon_supplier.get_analytics_stock_on_warehouse(
           limit=request_args['limit'], offset=request_args['offset']
        ))

        answer = (await analytics_stock_on_warehouse)
        return answer

    def get_model_instance(self, response: TResponse) -> TBaseModel:
        """Получение экземпляра модели из респонса"""
        print(response)
        entity = self._comparisons.parser_response_to_model(response=response)
        return entity
        
    def get_request_args(self) -> dict:
        """Получение аргументов для создание request запроса"""
        pass

    def _set_comparisons_schemes_to_models(self, comparisons_schemes_to_models: dict[Type[TResponse], Type[TBaseModel]]) -> ComparisonsSchemesToModels:
        """Задать map для сопоставления респонса и модели"""
        
        self.comparisons_schemes_to_models: dict[Type[TResponse], Type[TBaseModel]] = {
            
            AnalyticsStockOnWarehouseResponse: AnalyticsStockOnWarehouseModel,
            TotalItem: AnalyticsTotalItemModel,
            WhItem: AnalyticsWhItemModel,
            wh_item_item: AnalyticsWhItemItemModel,
            total_item_stock: AnalyticsTotalItemStocksModel,
            wh_item_stock: AnalyticsWhItemItemStockModel,
            
        }
        comparisons = ComparisonsSchemesToModels(self.comparisons_schemes_to_models)
        
        return comparisons


    

    async def start(self,):
        """Метод, который начинает процесс получения и сохранения данных"""
        limit = 1000
        offset = 0
        answer = (await self.get_response(
                {
                    'limit' : limit,
                    'offset':offset
                }
                ))
        entity = self.get_model_instance(answer)
        self.save_model_instance(entity=entity)
  
        #save model
        while len(answer.total_items) != 0:
            limit += 1000
            offset += 1000

            answer = (await self.get_response(
                {
                    'limit' : limit,
                    'offset':offset
                }
                ))
            
            entity = self.get_model_instance(answer)
            self.save_model_instance(entity=entity)
           
        return True
