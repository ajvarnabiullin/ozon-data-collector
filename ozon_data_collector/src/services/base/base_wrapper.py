from ozon_db.orm import ComparisonsSchemesToModels
from abc import ABC, abstractmethod, abstractproperty
from ...models.base import BaseModel
from typing import TypeVar, Type
from pydantic import BaseModel as PBaseModel
from ozon_sdk.ozon_api import OzonApi
from ...models import Seller
from ozon_db.core.db_wrapper import DBWrapper
from ozon_db.core.connection import DBContext

TBaseModel = TypeVar('TBaseModel', bound=BaseModel)
TResponse = TypeVar('TResponse', bound=PBaseModel)

class BaseWrapper(ABC):
    """Базовый класс для обертки Response в Model"""

    def __init__(self, client_id, api_key):
        self._ozon_supplier = OzonApi(client_id=client_id, api_key=api_key)
        self._db_context = DBContext()
        self._seller = self.get_seller_by_api_key_and_client_id(client_id =  client_id, api_key = api_key)
        self._db_wrapper = None
        
        #TODO: получение объекта продавца
    
    
    
    @abstractproperty
    def get_response(self, ) -> TResponse:
        """Получение аргументов для создания запросов"""
        pass

    @abstractproperty
    def get_request_args(self) -> dict:
        """Получение аргументов для создание request запроса"""
        pass

    @abstractmethod
    def _set_comparisons_schemes_to_models(self) -> dict[Type[TResponse], Type[TBaseModel]]:
        """Задать map для сопоставления респонса и модели"""
        pass

    def save_model_instance(self, entity: TBaseModel) -> TBaseModel:
        """Сохранить экземпляр модели в бд"""
        entity.seller_id = self._seller.pk
        self._db_context.add(entity)
        #TODO: поменять работу с моделями через dp_wrapper.py

        return entity

    @abstractmethod
    def start(self,):
        """Метод, который начинает процесс получения и сохранения данных"""
        pass
    
    def get_seller_by_api_key_and_client_id(self, api_key: str, client_id: str):
        # self._db_wrapper = DBWrapper(self._db_context, Seller)
        with self._db_context.session() as s:
            entity = s.query(Seller).where(Seller.api_key == api_key).where(Seller.client_id == client_id).first()
        
        return entity