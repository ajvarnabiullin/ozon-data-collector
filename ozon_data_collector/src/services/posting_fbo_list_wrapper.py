from .base import BaseWrapper
from ozon_db.orm import ComparisonsSchemesToModels
from ..models.base import BaseModel
from typing import TypeVar, Type
from pydantic import BaseModel as PBaseModel
from ozon_sdk.ozon_api import OzonApi
import asyncio
from ..models import *
from ozon_sdk.entities.posting_fbo_list import PostingFBOList, AdditionalData, FboPostingFboPostingAnalyticsData, PostingProduct, FinacialData
from ozon_sdk.entities.posting_financial_data import PostingFinancialDataProduct, ProductPicking, PostingFinancialDataServices
import datetime

TBaseModel = TypeVar('TBaseModel', bound=BaseModel)
TResponse = TypeVar('TResponse', bound=PBaseModel)

class PostingFBOListWrapper(BaseWrapper):
    """Список транзакций (версия 3)"""

    def __init__(self, client_id, api_key):
        super().__init__(client_id=client_id, api_key=api_key)

        self._comparisons_schemes_to_models = {

        }
        self._comparisons = self._set_comparisons_schemes_to_models(self._comparisons_schemes_to_models)

    
    async def get_response(self, request_args: dict = {}) -> TResponse:
        """Получение аргументов для создания запросов"""

        posting_fbo_list = asyncio.create_task(self._ozon_supplier.get_posting_fbo_list(
           dir=request_args['dir'],
               since=request_args[ "since"],
                status= "",
                to=request_args[ "to"],
                limit=request_args[ "limit"],
                offset=request_args[ "offset"],
                translit= True,
        
                analytics_data= True,
                financial_data= True
        ))

        answer = (await posting_fbo_list)
        return answer

    def get_model_instance(self, response: TResponse) -> TBaseModel:
        """Получение экземпляра модели из респонса"""
        entity = self._comparisons.parser_response_to_model(response=response)
        return entity
        
    def get_request_args(self) -> dict:
        """Получение аргументов для создание request запроса"""
        pass

    def _set_comparisons_schemes_to_models(self, comparisons_schemes_to_models: dict[Type[TResponse], Type[TBaseModel]]) -> ComparisonsSchemesToModels:
        """Задать map для сопоставления респонса и модели"""
        self.comparisons_schemes_to_models: dict[Type[TResponse], Type[TBaseModel]] = {

        AdditionalData: PostingFBOListAdditionalDataModel,
        FboPostingFboPostingAnalyticsData: PostingFBOListAnalyticsDataModel,
        FinacialData: PostingFBOListFinancialDataModel,
        PostingFinancialDataServices: PostingFBOListFinancialDataPostingServicesModel,
        PostingFinancialDataProduct: PostingFBOListFinancialDataProductModel,
        ProductPicking:ProductFBOListFinancialDataProductPickingModel,
        PostingFBOList:PostingFBOListModel,
        PostingProduct:PostingFBOListProductModel

        }
        comparisons = ComparisonsSchemesToModels(self.comparisons_schemes_to_models)
        return comparisons


    

    async def start(self,):
        """Метод, который начинает процесс получения и сохранения данных"""
        today = datetime.datetime.now(datetime.timezone.utc)
        three_month_ago = today-datetime.timedelta(days=90)
        since = str(three_month_ago.isoformat())
        to=str(today.isoformat())
        limit = 1000
        offset = 0
        answer = (await self.get_response(
            {
                "dir": "ASC",
                "since": since,
                "status": "",
                "to": to,
                "limit": limit,
                "offset": offset,
                "translit": True,
        
                "analytics_data": True,
                "financial_data": True
                
            }
        )
        )
        for result in answer.result:
            entity = self.get_model_instance(result)
            self.save_model_instance(entity=entity)

        #save model
        while len(answer.result) != 0:
            limit += 1000
            offset += 1000
            
            answer = (await self.get_response({
                 "dir": "ASC",
                "since": since,
                "status": "",
                "to": to,
                "limit": limit,
                "offset": offset,
                "translit": True,
        
                "analytics_data": True,
                "financial_data": True
            }))
            for result in answer.result:
                entity = self.get_model_instance(result)
            self.save_model_instance(entity=entity)
  
           
        return True
