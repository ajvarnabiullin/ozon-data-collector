from .base import BaseWrapper
from ozon_db.orm import ComparisonsSchemesToModels
from ..models.base import BaseModel
from typing import TypeVar, Type
from pydantic import BaseModel as PBaseModel
from ozon_sdk.ozon_api import OzonApi
import asyncio
from ..models import *
from ozon_sdk.entities.operation_posting import OperationPosting
from ozon_sdk.entities.finance_transaction_list import *
from ozon_sdk.entities.finance_transaction_list_v3_response_operation import Item as fin_transaction_item
from ozon_sdk.entities.operation_service import OperationService
import datetime

TBaseModel = TypeVar('TBaseModel', bound=BaseModel)
TResponse = TypeVar('TResponse', bound=PBaseModel)

class FinanceTransactionListWrapper(BaseWrapper):
    """Список транзакций (версия 3)"""

    def __init__(self, client_id, api_key):
        super().__init__(client_id=client_id, api_key=api_key)

        self._comparisons_schemes_to_models = {

        }
        self._comparisons = self._set_comparisons_schemes_to_models(self._comparisons_schemes_to_models)

    
    async def get_response(self, request_args: dict = {}) -> TResponse:
        """Получение аргументов для создания запросов"""

        finance_transaction_list = asyncio.create_task(self._ozon_supplier.get_finance_transaction_list(
                 _from = request_args['_from'],
                to = request_args['to'],
                operation_type = request_args['operation_type'],
                posting_number = request_args['posting_number'],
                transaction_type= request_args['transaction_type'],
                page = request_args['page'],
                page_size = request_args['page_size'],
        ))

        answer = (await finance_transaction_list)
        return answer

    def get_model_instance(self, response: TResponse) -> TBaseModel:
        """Получение экземпляра модели из респонса"""
        entity = self._comparisons.parser_response_to_model(response=response)
        return entity
        
    def get_request_args(self) -> dict:
        """Получение аргументов для создание request запроса"""
        pass

    def _set_comparisons_schemes_to_models(self, comparisons_schemes_to_models: dict[Type[TResponse], Type[TBaseModel]]) -> ComparisonsSchemesToModels:
        """Задать map для сопоставления респонса и модели"""
        
        self.comparisons_schemes_to_models: dict[Type[TResponse], Type[TBaseModel]] = {

            FinanceTransactionList: FinanceTransactionListModel,
            OperationPosting: FinanceTransactionListResponseOperationPostingModel,
            fin_transaction_item: FinanceTransactionListResponseOperationItemModel,
            OperationService: FinanceTransactionListResponseOperationServicesModel,
            FinanceTransactionListV3ResponseOperation: FinanceTransactionListResponseOperationModel,

        }
        comparisons = ComparisonsSchemesToModels(self.comparisons_schemes_to_models)
        
        return comparisons


    

    async def start(self,):
        """Метод, который начинает процесс получения и сохранения данных"""
        today = datetime.datetime.now(datetime.timezone.utc)
        three_month_ago = today-datetime.timedelta(days=90)
        page = 1
        page_size = 1000
        _from = str(three_month_ago.isoformat())
        to=str(today.isoformat())
        operation_type = [
                        'ClientReturnAgentOperation' ,
                        'MarketplaceMarketingActionCostOperation' ,
                        'MarketplaceSaleReviewsOperation',
                        'MarketplaceSellerCompensationOperation' ,
                        'OperationAgentDeliveredToCustomer',
                        'OperationAgentDeliveredToCustomerCanceled' ,
                        'OperationAgentStornoDeliveredToCustomer' ,
                        'OperationClaim',
                        'OperationCorrectionSeller',
                        'OperationDefectiveWriteOff' ,
                        'OperationItemReturn',
                        'OperationLackWriteOff' ,
                        'OperationMarketplaceCrossDockServiceWriteOff',
                        'OperationMarketplaceServiceStorage' ,
                        'OperationSetOff' ,
                        'MarketplaceSellerReexposureDeliveryReturnOperation' ,
                        'OperationReturnGoodsFBSofRMS',
                        'ReturnAgentOperationRFBS' ,
                        'MarketplaceSellerShippingCompensationReturnOperation' ,
                        'OperationMarketplaceServicePremiumCashback' ,
                    ]

        page=1
        page_size=1000
  
        answer = (await self.get_response(
            {
                '_from' : _from,
                'to':to,
                'operation_type':operation_type,
                'posting_number':'',
                'transaction_type':'all',
                'page':page,
                'page_size':page_size
            }
        )
        ).result
        entity = self.get_model_instance(answer)
        self.save_model_instance(entity=entity)

        #save model
        while len(answer.operations) != 0:
            page += 1
            page_size += 1000
            
            answer = (await self.get_response(
                {
                    '_from' : _from,
                    'to':to,
                    'operation_type':operation_type,
                    'posting_number':'',
                    'transaction_type':'all',
                    'page':page,
                    'page_size':page_size
                }
                )).result
            entity = self.get_model_instance(answer)
            self.save_model_instance(entity=entity)
    
           
        return True
