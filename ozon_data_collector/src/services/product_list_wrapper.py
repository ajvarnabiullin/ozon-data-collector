from .base import BaseWrapper
from ozon_db.orm import ComparisonsSchemesToModels
from abc import ABC, abstractmethod, abstractproperty
from ..models.base import BaseModel
from typing import TypeVar, Type
from pydantic import BaseModel as PBaseModel
from ozon_sdk.ozon_api import OzonApi
import asyncio
from ozon_sdk.entities.product_list import ProductList, ProductListItems
from ..models import *

import datetime

TBaseModel = TypeVar('TBaseModel', bound=BaseModel)
TResponse = TypeVar('TResponse', bound=PBaseModel)

class ProductListWrapper(BaseWrapper):
    """Список транзакций (версия 3)"""

    def __init__(self, client_id, api_key):
        super().__init__(client_id=client_id, api_key=api_key)

        self._comparisons_schemes_to_models = {

        }
        self._comparisons = self._set_comparisons_schemes_to_models(self._comparisons_schemes_to_models)

    
    async def get_response(self, request_args: dict = {}) -> TResponse:
        """Получение аргументов для создания запросов"""

        product_list = asyncio.create_task(self._ozon_supplier.get_product_list(
           last_id = request_args['last_id'],
                 limit = request_args['limit'],
        ))

        answer = (await product_list)
        return answer

    def get_model_instance(self, response: TResponse) -> TBaseModel:
        """Получение экземпляра модели из респонса"""
        entity = self._comparisons.parser_response_to_model(response=response)
        return entity
        
    def get_request_args(self) -> dict:
        """Получение аргументов для создание request запроса"""
        pass

    def _set_comparisons_schemes_to_models(self, comparisons_schemes_to_models: dict[Type[TResponse], Type[TBaseModel]]) -> ComparisonsSchemesToModels:
        """Задать map для сопоставления респонса и модели"""
        self.comparisons_schemes_to_models: dict[Type[TResponse], Type[TBaseModel]] = {

        ProductListItems: ProductListItemsModel,
        ProductList: ProductListModel,
        

        }
        comparisons = ComparisonsSchemesToModels(self.comparisons_schemes_to_models)
        return comparisons


    

    async def start(self,):
        """Метод, который начинает процесс получения и сохранения данных"""
        last_id = ''
        limit = 1000
       
        answer = (await self.get_response(
            {
               'last_id': last_id,
               'limit': limit
                
            }
        )
        ).result
        
        entity = self.get_model_instance(answer)
        self.save_model_instance(entity=entity)
        print(entity)
        #save model
        while len(answer.items) != 0:
            limit += 1000
            last_id = answer.last_id
            
            answer = (await self.get_response({
                 'last_id': last_id,
                 'limit': limit
            })).result
            entity = self.get_model_instance(answer)
            self.save_model_instance(entity=entity)
            print(entity)
           
        return True
