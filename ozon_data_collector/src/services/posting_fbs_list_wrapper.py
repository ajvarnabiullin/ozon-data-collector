from .base import BaseWrapper
from ozon_db.orm import ComparisonsSchemesToModels
from abc import ABC, abstractmethod, abstractproperty
from ..models.base import BaseModel
from typing import TypeVar, Type
from pydantic import BaseModel as PBaseModel
from ozon_sdk.ozon_api import OzonApi
import asyncio
import datetime
from ..models import *
from ozon_sdk.entities.posting_fbs_list import FBSPosting
from ozon_sdk.entities.fbs_posting import Addressee, Barcodes, Cancellation, Customer, DeliveryMethod, FPSPostingAnalyticsData, FinacialData, FBSPostingProduct, FBSPostingRequirements
from ozon_sdk.entities.posting_financial_data import PostingFinancialDataProduct, PostingFinancialDataServices, ProductPicking

TBaseModel = TypeVar('TBaseModel', bound=BaseModel)
TResponse = TypeVar('TResponse', bound=PBaseModel)

class PostingFBSListWrapper(BaseWrapper):
    """Сохранение Отчёт по остаткам и товарам в бд"""

    def __init__(self, client_id, api_key):
        super().__init__(client_id=client_id, api_key=api_key)

        self._comparisons_schemes_to_models = {

        }
        self._comparisons = self._set_comparisons_schemes_to_models(self._comparisons_schemes_to_models)

    
    async def get_response(self, request_args: dict = {}) -> TResponse:
        """Получение аргументов для создания запросов"""

        posting_fbs_list = asyncio.create_task(self._ozon_supplier.get_posting_fbs_list(
           limit=request_args['limit'], offset=request_args['offset'],
           translit=True, analytics_data=True, financial_data=True, barcodes=True, 
           dir='ASC', since=request_args['since'], to=request_args['to']
        ))

        answer = (await posting_fbs_list)
        return answer

    def get_model_instance(self, response: TResponse) -> TBaseModel:
        """Получение экземпляра модели из респонса"""
        print(response)
        entity = self._comparisons.parser_response_to_model(response=response)
        return entity
        
    def get_request_args(self) -> dict:
        """Получение аргументов для создание request запроса"""
        pass

    def _set_comparisons_schemes_to_models(self, comparisons_schemes_to_models: dict[Type[TResponse], Type[TBaseModel]]) -> ComparisonsSchemesToModels:
        """Задать map для сопоставления респонса и модели"""
        self.comparisons_schemes_to_models: dict[Type[TResponse], Type[TBaseModel]] = {
        FBSPosting: PostingFBSListModel,
        Barcodes: PostingFBSListPostingBarcodesModel,
        Addressee: PostingFBSListPostingAddresseeModel,
        Cancellation: PostingFBSListPostingCancellation,
        Customer: PostingFBSListPostingCustomerModel,
        DeliveryMethod: PostingFBSListPostingDeliveryMethodModel,
        FPSPostingAnalyticsData: PostingFBSListAnalyticsDataModel,
        FinacialData:PostingFBSListPostingFinancialDataModel,
        FBSPostingProduct:PostingFBSListPostingFinancialDataProductModel,
        FBSPostingRequirements:PostingFBSListPostingRequirementsModel,
        PostingFinancialDataProduct: PostingFBSListPostingFinancialDataProductModel,
        PostingFinancialDataServices: PostingFBOListFinancialDataPostingServicesModel,
        ProductPicking: PostingFBSListPostingFinancialDataProductPickingModel,

        }
        comparisons = ComparisonsSchemesToModels(self.comparisons_schemes_to_models)
        return comparisons


    

    async def start(self,):
        """Метод, который начинает процесс получения и сохранения данных"""
        limit = 1000
        offset = 0
        today = datetime.datetime.now(datetime.timezone.utc)
        three_month_ago = today-datetime.timedelta(days=90)
        answer = (await self.get_response({
            
                "since": str(three_month_ago.isoformat()),
                "to": str(today.isoformat()),
            
            "limit": 1000,
            "offset": 0,
            "translit": True,
            
                "analytics_data": True,
                "financial_data": True
            

        })).result
        if answer.postings == []:
            return True
        entity = self.get_model_instance(answer)
        self.save_model_instance(entity=entity)
  
        #save model
        while len(answer.total_items) != 0:
            limit += 1000
            offset += 1000

            answer = (await self.get_response({'limit' : limit,
            'offset':offset, "since": str(three_month_ago.isoformat()),
                "to": str(today.isoformat()),})).result
            entity = self.get_model_instance(answer)
            self.save_model_instance(entity=entity)
           

        return True
