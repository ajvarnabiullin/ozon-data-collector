from ozon_db.core.connection import DBContext
from ozon_db.core.db_wrapper import DBWrapper
from ozon_data_collector.src.models import Seller
from ozon_data_collector.src.services import AnalyticsOnStockWarehouseWrapper, FinanceTransactionListWrapper, ProductListWrapper, PostingFBOListWrapper, ProductInfoListWrapper, ProductInfoStocksByWarehouseWrapper, ProductInfoStocksWrapper, PostingFBSListWrapper
from ozon_sdk.ozon_api import OzonApi
from ozon_db.core import create_db
import asyncio

def main():
    # create_db.create_db('ozon_db5')
    db_context = DBContext()
    db_wrapper = DBWrapper(db_context, Seller)
    print(db_context)
    print('Create')
    # seller = Seller(
    #     api_key='',
    #     client_id=''
    # )
    # db_wrapper.add(seller)
    client_id=''
    api_key=''
    analytics_wrapper = AnalyticsOnStockWarehouseWrapper(client_id=client_id, api_key=api_key)
    asyncio.get_event_loop().run_until_complete(analytics_wrapper.start())

    financial_wrapper = FinanceTransactionListWrapper(client_id=client_id, api_key=api_key)
    asyncio.get_event_loop().run_until_complete(financial_wrapper.start())

    product_list_wrapper = ProductListWrapper(client_id=client_id, api_key=api_key)
    asyncio.get_event_loop().run_until_complete(product_list_wrapper.start())

    product_info_list_wrapper = ProductInfoListWrapper(client_id=client_id, api_key=api_key)
    asyncio.get_event_loop().run_until_complete(product_info_list_wrapper.start())

    posting_fbo_list_wrapper = PostingFBOListWrapper(client_id=client_id, api_key=api_key)
    asyncio.get_event_loop().run_until_complete(posting_fbo_list_wrapper.start())

    product_info_stocks_wrapper = ProductInfoStocksWrapper(client_id=client_id, api_key=api_key)
    asyncio.get_event_loop().run_until_complete(product_info_stocks_wrapper.start())

    product_info_stocks_by_warehouse_wrapper = ProductInfoStocksByWarehouseWrapper(client_id=client_id, api_key=api_key)
    asyncio.get_event_loop().run_until_complete(product_info_stocks_by_warehouse_wrapper.start())

    posting_fbs_list_wrapper = PostingFBSListWrapper(client_id=client_id, api_key=api_key)
    asyncio.get_event_loop().run_until_complete(posting_fbs_list_wrapper.start())


main()
